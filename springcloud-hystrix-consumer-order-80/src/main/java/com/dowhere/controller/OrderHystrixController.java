package com.dowhere.controller;

import com.dowhere.service.OrderHystrixService;
import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author: Jessica
 * @create: 2022-04-27 7:38
 * @desc:
 **/
@RestController
@Slf4j
//这个需要搭配 @HystrixCommand 才有效果
@DefaultProperties(defaultFallback = "payment_Global_FallbackMethod")
public class OrderHystrixController {

    @Resource
    private OrderHystrixService orderHystrixService;

    @GetMapping("/consumer/payment/hystrix/ok/{id}")
    public String paymentInfo_OK(@PathVariable("id") Integer id) {
        return orderHystrixService.paymentInfo_OK(id);
    }

    @GetMapping("/consumer/payment/hystrix/timeout/{id}")
//    @HystrixCommand(fallbackMethod = "OrderTimeOutFallbackMethod", commandProperties = {
//            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "1500") //客户端只等待服务端1.5s
//    })
    @HystrixCommand
    public String paymentInfo_TimeOut(@PathVariable("id") Integer id){
        return orderHystrixService.paymentInfo_TimeOut(id);
    }

    public String OrderTimeOutFallbackMethod(@PathVariable("id") Integer id) {
        return "我是消费者80, 对方支付系统繁忙请稍后再试或者自己运行出错请检查自己-.-";
    }

    //全局fallback 全局方法不能有参数，否则会报找不到方法的异常
    public String payment_Global_FallbackMethod() {
        return "Global异常信息处理，请稍后再试";
    }





}
