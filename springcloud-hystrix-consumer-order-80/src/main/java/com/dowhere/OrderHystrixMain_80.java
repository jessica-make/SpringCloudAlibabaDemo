package com.dowhere;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author: Jessica
 * @create: 2022-04-27 7:33
 * @desc:
 **/
@SpringBootApplication
@EnableFeignClients
@EnableCircuitBreaker
public class OrderHystrixMain_80 {
    public static void main(String[] args) {
        SpringApplication.run(OrderHystrixMain_80.class, args);
    }
}
