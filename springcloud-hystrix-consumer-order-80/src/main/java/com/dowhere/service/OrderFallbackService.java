package com.dowhere.service;

import org.springframework.stereotype.Component;

/**
 * @author: Jessica
 * @create: 2022-04-27 9:33
 * @desc:
 **/
@Component
public class OrderFallbackService implements OrderHystrixService {

    @Override
    public String paymentInfo_OK(Integer id) {
        return "------OrderFallbackService ----paymentInfo_OK fallback";
    }

    @Override
    public String paymentInfo_TimeOut(Integer id) {
        return "------OrderFallbackService ----paymentInfo_TimeOut fallback";
    }
}
