package com.dowhere.controller;

import com.dowhere.entity.CommonResult;
import com.dowhere.entity.Payment;
import com.dowhere.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author: Jessica
 * @create: 2022-04-24 20:02
 * @desc:
 **/

@RestController
@Slf4j
public class PaymentController {
    @Resource
    private PaymentService paymentService;

    @Resource
    private DiscoveryClient discoveryClient;

    @Value("${server.port}")
    private String serverPort;

    @PostMapping(value = "/payment/create")
    public CommonResult<?> create(@RequestBody Payment payment) {
        log.info("前端传递数据："+payment);
        int result = paymentService.create(payment);
        log.info("************************插入结果：" +result);
        if (result > 0) {
            return new CommonResult<>(200,"插入数据库成功",result);
        } else {
            return new CommonResult<>(500,"插入数据库失败", null);
        }
    }

    @GetMapping(value = "/payment/get/{id}")
    public CommonResult<?> getPaymentById(@PathVariable("id") Long id) {
        Payment payment = paymentService.getPaymentById(id);
        log.info("*************************查询结果：" +payment);
        if (payment != null) {
            return new CommonResult<>(8001, "查询成功", payment);
        } else {
            return new CommonResult<>(500,"没有对应id: "+id+" 的记录", null);
        }
    }

    @RequestMapping("/payment/discovery")
    public Object discovery(){
        List<String> services = discoveryClient.getServices();
        for (String service : services) {
            System.out.println("service "+service);
        }
        return services;
    }

    @GetMapping(value = "/payment/lb")
    public String getPayment(){
        return serverPort;
    }


    @GetMapping(value = "/payment/zipkin")
    public String getPaymentZipKin(){
        return "I am zipkin";
    }


    /**
     * 模拟feign超时
     *
     * @return
     */
    @GetMapping(value = "/payment/feign/timeout")
    public String paymentFeignTimeout(){
        try {
            TimeUnit.SECONDS.sleep(3);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
        return serverPort;
    }
}

