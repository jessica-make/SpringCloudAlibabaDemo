package com.dowhere;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @author: Jessica
 * @create: 2022-04-28 17:51
 * @desc:
 **/
@SpringBootApplication
@EnableEurekaClient
public class ConfigClientMain_3366 {
    public static void main(String[] args) {
        SpringApplication.run(ConfigClientMain_3366.class);
    }
}
