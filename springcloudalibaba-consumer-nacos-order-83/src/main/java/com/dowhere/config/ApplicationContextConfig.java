package com.dowhere.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @author: Jessica
 * @create: 2022-04-30 9:29
 * @desc:
 **/
@Configuration
public class ApplicationContextConfig {
    @Bean
    @LoadBalanced//restTemplate只管调用，本身不具备负载均衡功能，要用@LoadBalanced注解赋予restTemplate负载均衡的功能
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }
}
