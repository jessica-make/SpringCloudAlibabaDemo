package com.dowhere;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author: Jessica
 * @create: 2022-04-30 9:23
 * @desc:
 **/

@EnableDiscoveryClient
@SpringBootApplication
public class OrderNacosMain_83 {
    public static void main(String[] args) {
        SpringApplication.run(OrderNacosMain_83.class, args);
    }
}
