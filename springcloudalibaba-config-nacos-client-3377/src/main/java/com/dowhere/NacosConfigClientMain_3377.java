package com.dowhere;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author: Jessica
 * @create: 2022-04-30 10:06
 * @desc:
 **/
@EnableDiscoveryClient
@SpringBootApplication
public class NacosConfigClientMain_3377 {
    public static void main(String[] args) {
        SpringApplication.run(NacosConfigClientMain_3377.class, args);
    }
}
