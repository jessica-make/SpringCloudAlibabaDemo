package com.dowhere.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: Jessica
 * @create: 2022-04-30 10:11
 * @desc:
 **/
@RestController
@RefreshScope //支持Nacos的动态刷新功能 通过springcloud原生注解@RefreshScope实现配置自动更新
public class ConfigClientController {

    @Value("${config.info}")
    private String configInfo;

    @GetMapping("/config/info")
    public String getConfigInfo() {
        return configInfo;
    }

}
