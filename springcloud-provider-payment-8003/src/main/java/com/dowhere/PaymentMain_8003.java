package com.dowhere;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @author: Jessica
 * @create: 2022-04-26 11:39
 * @desc:
 **/

@SpringBootApplication(scanBasePackages = {"com.dowhere.*"})
@MapperScan(basePackages = "com.dowhere.mapper")
@EnableEurekaClient
public class PaymentMain_8003 {
    public static void main(String[] args) {
        SpringApplication.run(PaymentMain_8003.class,args);
    }
}
