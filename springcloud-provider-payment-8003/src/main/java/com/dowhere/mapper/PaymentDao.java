package com.dowhere.mapper;

import com.dowhere.entity.Payment;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author: Jessica
 * @create: 2022-04-24 19:49
 * @desc:
 **/

@Mapper
public interface PaymentDao {

    int create(Payment payment);

    Payment getPaymentById(@Param("id") Long id);
}
