package com.dowhere.service.impl;

import com.dowhere.entity.Payment;
import com.dowhere.mapper.PaymentDao;
import com.dowhere.service.PaymentService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author: Jessica
 * @create: 2022-04-24 19:53
 * @desc:
 **/
@Service
public class PaymentServiceImpl implements PaymentService {

    @Resource
    private PaymentDao paymentDao;

    @Override
    public int create(Payment payment) {
        return paymentDao.create(payment);
    }

    @Override
    public Payment getPaymentById(Long id) {
        return paymentDao.getPaymentById(id);
    }
}
