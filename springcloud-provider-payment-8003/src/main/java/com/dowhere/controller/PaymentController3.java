package com.dowhere.controller;

import com.dowhere.entity.CommonResult;
import com.dowhere.entity.Payment;
import com.dowhere.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author: Jessica
 * @create: 2022-04-26 17:28
 * @desc:
 **/

@RestController
@Slf4j
public class PaymentController3 {

    @Resource
    private PaymentService paymentService;

    @Value("${server.port}")
    private String serverPort;


    @PostMapping(value = "/payment/create")
    public CommonResult<?> create(@RequestBody Payment payment) {
        log.info("前端传递数据："+payment);
        int result = paymentService.create(payment);
        log.info("************************插入结果：" +result);
        if (result > 0) {
            return new CommonResult<>(200,"插入数据库成功",result);
        } else {
            return new CommonResult<>(500,"插入数据库失败", null);
        }
    }

    @GetMapping(value = "/payment/get/{id}")
    public CommonResult<?> getPaymentById(@PathVariable("id") Long id) {
        Payment payment = paymentService.getPaymentById(id);
        log.info("*************************查询结果：" +payment);
        if (payment != null) {
            return new CommonResult<>(8003, "查询成功", payment);
        } else {
            return new CommonResult<>(500,"没有对应id: "+id+" 的记录", null);
        }
    }



    @RequestMapping("/payment/lb")
    public String getServerPort(){
        return serverPort;
    }

}
