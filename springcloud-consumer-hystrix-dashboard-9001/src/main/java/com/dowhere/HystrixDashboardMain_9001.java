package com.dowhere;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;

/**
 * @author: Jessica
 * @create: 2022-04-27 14:55
 * @desc:
 **/

@SpringBootApplication
@EnableHystrixDashboard
public class HystrixDashboardMain_9001 {
    public static void main(String[] args) {
        SpringApplication.run(HystrixDashboardMain_9001.class, args);
    }
}
