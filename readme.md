1、CSDN地址,百度方便使用 
blog.csdn.net

2、Eureka-client 每30秒发送一次心跳，相当于交租金
   Eureka-Server 90秒没有收到租金，它会认为没有Eureka-client续约成功，剔除服务
   
3、Eureka-client 在关机时向Eureka-Server 发送一个取消请求。
这将从服务器的实例注册表中删除实例，从而有效地将实例从流量中取出

4、consul 查看版本
consul --version

5、consul 启动
consul agent -dev

6、访问本地consul
http://localhost:8500/ui/dc1/services

7、本地伪集群修改地址，新增主机映射
C:\Windows\System32\drivers\etc

127.0.0.1 eureka7001.com
127.0.0.1 eureka7002.com
127.0.0.1 eureka7003.com

8、服务降级 fallback
   服务熔断 break
   服务限流 limit
   
9、springboot内嵌的tomcat 
  默认线程池数量 10个
  
10、断路器开启或关闭的条件：
   1.当满足一定的请求总数阈值的时候（默认10秒内超过20个请求）
   2.当失败率达到一定的时候（默认10秒内超过50%的请求失败）
   3.达到以上阈值，断路器将会开启，当开启的时候，所有的请求都不会进行转发。
   4.一段时间之后（默认5秒），这个时候断路器处于半开状态，会让其中一个请求进行转发。
   如果成功，断路器会关闭，若失败，继续开启，重复。

11、GateWay 基于WebFlux框架实现，
而WebFlux框架底层则使用了高性能的Reactor模式通信框架Netty。

12、Spring Cloud GateWay的目标是提供统一的路由方式
且基于Filter链的方式提供网关的基本功能，例如：安全、监控/指标、限流。

13、bootstrap.yml是什么：
   application.yml是用户级的资源配置项，bootstrap.yml是系统级的，优先级更高。
   
   SpringCloud会创建一个"BootStrap Context"，作为Spring应用的"Application Context"的父上下文。
   初始化的时候，“BootStrap Context” 负责从外部源加载配置属性并解析配置
   。这两个上下文共享一个从外部获取的 “Environment”。
   
   "BootStrap Context"属性有高优先级，默认情况下，它们不会被本地配置覆盖。
   “BootStrap Context” 和 "Application Context"有着不同的约定，
   所以增加了一个 bootstrap.yml 文件，保证 Bootstrap Context 和 
   Application Context 配置的分离

14、bootstrap.yml 没有小树叶标志，可以先建启动类

15、RabbitMq 本地插件安装，进入sbin目录下
rabbitmq-plugins enable rabbitmq_management

16、RabbitMq 本地启动

17、设计主键思路
时间戳+机器ID+进程Id+计数器

18、消息传递在RabbitMQ是Exchange，在Kafka中是Topic和Partition
RabbitMQ 同一个Group组不会被重复消费
Kafka 同一个Partition不会被重复消费 

19、zipkin
TranceId 一条链路
Span ID 当前路径ID
parentId 上一条路径ID

------------------------------------------------------------------------------------->
20、Nacos支持Ap和Cp,默认Ap,如果需要切换
curl -X PUT '$NACOS_SERVER:8848/nacos/v1/ns/operator/switches?entry=serverMode&value=CP

21、Sentinel 冷加载
默认codeFactor为3，即请求QPS从threshold/3开始，经过预热时长逐渐上升至设定的QPS阈值

22、Sentinel 持久化
[
  {
    "resource": "/rateLimit/byUrl",
    "IimitApp": "default",
    "grade": 1,
    "count": 1, 
    "strategy": 0,
    "controlBehavior": 0,
    "clusterMode": false
  }
]

resource: 资源名称
limitApp: 来源应用
grade: 阈值类型，0表示线程数，1表示QPS
count: 单机阈值
strategy: 流控模式，0表示直接，1表示关联，2表示链路
controlBehavior: 流控效果，0表示快速失败，1表示Warm Up，2表示排队等待
clusterMode: 是否集群

23、Feign的默认超时时间 1s

------------------------------------------------------------------------------------>
                           Seata
24、分布式事务
一次业务操作需要跨多个数据源或需要跨多个系统进行远程调用，就会产生分布式事务

25、处理过程
1、TM向TC申请开启一个全局事务，全局事务创建成功并生成一个全局唯一的XID
2、XID在微服务调用链路的上下文中传播
3、RM向TC注册分支事务，将其纳入XID对全局事务的管辖
4、TM向TC发起针对XID的全局提交或回滚决议
5、TC调度XID下管辖的全部分支事务完成提交或者回滚请求

26、分布式唯一ID 的硬性要求
1.高可用：保证唯一性，单调递增
2.低时延：获取ID的速度要快
3.高吞吐：保证每秒能在W级别的生成速度

27、分布式唯一ID方案
1、UUID，36位字符串太长，无序，且索引占空间
2、Mysql,上百万Id同时需要生成(金融、电商)，Mysql需要先查一下数据库上一次自增Id，然后在自增，
非常消耗性能
3、Redis,4.0以前用单线程 原子操作INCR INRBY实现，单机版生成不足以满足上百万并发，
需要Redis集群生成，那么就需要初始化分不同段生成步长，缺点是需要保证Redis集群不宕机

4、雪花算法snowflake，
按照时间有序生成，每秒可以产生26W个自增可排序Id,
每个Id是个64bit的整数，转成Long类型，最多19位
分布式系统不会产生Id碰撞，由数据中心dataCenter和WorkerId区分

优点：Id自增，不依赖第三方系统，分配Id bit位可以自由分配，灵活高效

缺点:依赖时钟,如果机器时间回拨,会导致重复的ID生成

5、美团的Leaf-snowflake方案，采用了半依赖的方式，采用依赖zk发号的方式实现

6、UidGenerator
百度开源 一款分布式高性能的唯一ID生成器，是基于snowflake模型的一种ID生成器