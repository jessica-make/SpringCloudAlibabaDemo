package com.dowhere;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author: Jessica
 * @create: 2022-04-25 18:48
 * @desc:
 **/
@SpringBootApplication(scanBasePackages = {"com.dowhere.*"})
@EnableDiscoveryClient
public class PaymentMain_8004 {
    public static void main(String[] args) {
        SpringApplication.run(PaymentMain_8004.class,args);
    }
}
