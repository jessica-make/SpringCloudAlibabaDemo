package com.dowhere.service;

import com.dowhere.entity.CommonResult;
import com.dowhere.entity.Payment;
import org.springframework.stereotype.Component;

/**
 * @author: Jessica
 * @create: 2022-05-01 19:53
 * @desc:
 **/
@Component
public class PaymentFallbackService implements PaymentService{
    @Override
    public CommonResult<Payment> paymentSQL(Long id) {
        return new CommonResult<>(404,"服务降级返回,---PaymentFallbackService",new Payment(id,"error serial"));
    }
}
