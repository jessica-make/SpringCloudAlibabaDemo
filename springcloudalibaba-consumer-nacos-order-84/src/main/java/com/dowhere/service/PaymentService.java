package com.dowhere.service;

import com.dowhere.entity.CommonResult;
import com.dowhere.entity.Payment;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author: Jessica
 * @create: 2022-05-01 19:52
 * @desc:
 **/
@FeignClient(value = "nacos-payment-provider",fallback = PaymentFallbackService.class)
public interface PaymentService {
    // order调用paymentService
    @GetMapping(value = "/paymentSQL/{id}")
     CommonResult<Payment> paymentSQL(@PathVariable("id") Long id);
}
