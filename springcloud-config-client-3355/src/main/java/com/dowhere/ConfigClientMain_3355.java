package com.dowhere;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @author: Jessica
 * @create: 2022-04-28 9:31
 * @desc:
 **/

@EnableEurekaClient
@SpringBootApplication
public class ConfigClientMain_3355 {
    public static void main(String[] args) {
        SpringApplication.run(ConfigClientMain_3355.class);
    }
}
