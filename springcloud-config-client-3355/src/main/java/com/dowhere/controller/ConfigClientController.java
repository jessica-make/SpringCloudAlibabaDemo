package com.dowhere.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: Jessica
 * @create: 2022-04-28 9:39
 * @desc:
 **/
@RestController
@RefreshScope
@Slf4j
public class ConfigClientController {

    @Value("${spring.cloud.config.profile}")
    private String profile;

    @Value("${config.info}")
    private String configInfo;

    @GetMapping("/configInfo")
    public String getConfigInfo() {
        log.info("gitee的环境是:{},配置信息是: {}",profile,configInfo);
        return configInfo;
    }

}
