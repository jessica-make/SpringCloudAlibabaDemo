1、动态刷新配置 curl
curl -X POST "http://localhost:3355/actuator/refresh"

2、动态刷新配置 postman  POST请求
http://localhost:3355/configInfo

3、消息总线Bus通知两种方式
  一、直接通知Config-server 推荐
  二、通知其中一个Config-client，然后传播到其他Config-client
  
3A、RabbitMq 每个端口都有不同的作用，5672、15672、25672，
其中5672是提供消息队列服务所用，15672是开启后台http访问所用 
  4369 – erlang发现口 
  5672 --client端通信口
  15672 – 管理界面ui端口
  25672 – server间内部通信口
  
5、动态刷新配置，curl 多台机器同时生效
curl -X POST "http://localhost:3344/actuator/bus-refresh"
  
6、动态刷新配置 postman  POST请求  
http://localhost:3344/actuator/bus-refresh

7、动态定点刷新，只给3355, 尾缀是微服务名字+端口号
curl -X POST "http://localhost:3344/actuator/bus-refresh/springcloud-config-client:3355"
