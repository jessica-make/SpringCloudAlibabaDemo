package com.dowhere.lb;

import org.springframework.cloud.client.ServiceInstance;

import java.util.List;

/**
 * @author: Jessica
 * @create: 2022-04-26 15:45
 * @desc:
 **/

public interface LoadBalancer {

    ServiceInstance getInstances(List<ServiceInstance> serviceInstances);
}
