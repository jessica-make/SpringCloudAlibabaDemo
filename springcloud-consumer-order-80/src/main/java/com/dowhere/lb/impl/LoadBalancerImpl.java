package com.dowhere.lb.impl;

import com.dowhere.lb.LoadBalancer;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author: Jessica
 * @create: 2022-04-26 15:52
 * @desc:
 **/
@Component
public class LoadBalancerImpl implements LoadBalancer {
    //这里可以设置为final 因为while循环 操作的是主内存的值
    private final AtomicInteger atomicInteger = new AtomicInteger(0);

    public final int getAndIncrement(){
        int current,next;
        do {
            current = this.atomicInteger.get();
            next=current>=Integer.MAX_VALUE ? 0:current+1;
            //while里面是保证原子性，就是保证 this.atomicInteger.get()的值拿到后，没有被更新
        }while (!this.atomicInteger.compareAndSet(current,next));
        System.out.println("next: "+next);
        return next;
    }

    @Override
    public ServiceInstance getInstances(List<ServiceInstance> serviceInstances) {
        int index = getAndIncrement() % serviceInstances.size();
        return serviceInstances.get(index);
    }
}
