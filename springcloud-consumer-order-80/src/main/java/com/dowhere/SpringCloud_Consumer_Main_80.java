package com.dowhere;

import com.myrule.MyselfRule;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;

/**
 * @author: Jessica
 * @create: 2022-04-25 8:09
 * @desc:
 **/
@SpringBootApplication(scanBasePackages = {"com.dowhere.*"})
@EnableDiscoveryClient
@RibbonClient(name = "SPRINGCLOUD-PAYMENT-SERVICE",configuration = MyselfRule.class)
public class SpringCloud_Consumer_Main_80 {
    public static void main(String[] args) {
        SpringApplication.run(SpringCloud_Consumer_Main_80.class,args);
    }
}
