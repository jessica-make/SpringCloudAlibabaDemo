package com.myrule;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RoundRobinRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author: Jessica
 * @create: 2022-04-26 14:19
 * @desc:
 **/

@Configuration
public class MyselfRule {
    @Bean
    public IRule getMyRule(){
        //自己写的负载均衡算法，点击5次后，切换到下一个节点
        return new RoundRobinRule();
    }
}
