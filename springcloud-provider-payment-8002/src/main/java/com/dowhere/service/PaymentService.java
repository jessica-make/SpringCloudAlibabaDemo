package com.dowhere.service;

import com.dowhere.entity.Payment;
import org.apache.ibatis.annotations.Param;

/**
 * @author: Jessica
 * @create: 2022-04-24 19:54
 * @desc:
 **/

public interface PaymentService {
    int create(Payment payment);

    Payment getPaymentById(@Param("id") Long id);
}
