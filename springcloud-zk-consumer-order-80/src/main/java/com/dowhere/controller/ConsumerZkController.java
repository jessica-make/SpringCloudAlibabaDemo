package com.dowhere.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

/**
 * @author: Jessica
 * @create: 2022-04-25 20:12
 * @desc:
 **/
@RestController
@Slf4j
public class ConsumerZkController {

    private final static String PAYMENT_Zk_URL = "http://springcloud-provider-payment";

    @Resource
    private RestTemplate restTemplate;

    @RequestMapping("/consumer/zk")
    public String paymentZK() {
        return restTemplate.getForObject(PAYMENT_Zk_URL+"/payment/zk",String.class);
    }

}
