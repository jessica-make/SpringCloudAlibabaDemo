package com.dowhere;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author: Jessica
 * @create: 2022-04-25 20:11
 * @desc:
 **/

@SpringBootApplication
public class SpringCloud_Zk_Consumer_Main_80 {
    public static void main(String[] args) {
        SpringApplication.run(SpringCloud_Zk_Consumer_Main_80.class,args);
    }
}
