package com.dowhere.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @author: Jessica
 * @create: 2022-04-25 8:12
 * @desc:
 **/

@Configuration
public class ConsumerConfig {

    @Bean
    @LoadBalanced //服务名是同一个，多个服务提供者需要负载均衡
    public RestTemplate getRestTemplate(){
        return new RestTemplate();
    }

}
