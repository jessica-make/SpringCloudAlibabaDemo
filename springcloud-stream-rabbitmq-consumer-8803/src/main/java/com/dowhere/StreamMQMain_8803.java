package com.dowhere;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author: Jessica
 * @create: 2022-04-29 19:45
 * @desc:
 **/
@SpringBootApplication
public class StreamMQMain_8803 {
    public static void main(String[] args) {
        SpringApplication.run(StreamMQMain_8803.class, args);
    }
}
