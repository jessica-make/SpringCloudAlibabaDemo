package com.dowhere.config;

import feign.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author: Jessica
 * @create: 2022-04-26 19:25
 * @desc:
 **/
@Configuration
public class FeignConfig {
    /**
     * feignClient日志级别配置
     *
     * @return
     */
    @Bean
    public Logger.Level feignLoggerLevel() {
        // 请求和响应的头信息,请求和响应的正文及元数据
        return Logger.Level.FULL;
    }

}
