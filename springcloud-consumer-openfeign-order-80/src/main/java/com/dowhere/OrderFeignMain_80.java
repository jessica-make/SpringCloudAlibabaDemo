package com.dowhere;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author: Jessica
 * @create: 2022-04-26 18:04
 * @desc:
 **/

@SpringBootApplication(scanBasePackages = {"com.dowhere.*"})
@EnableDiscoveryClient
@EnableFeignClients
public class OrderFeignMain_80 {
    public static void main(String[] args) {
        SpringApplication.run(OrderFeignMain_80.class,args);
    }
}
