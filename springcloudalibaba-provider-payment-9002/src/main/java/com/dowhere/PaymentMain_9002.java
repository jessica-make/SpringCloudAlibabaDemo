package com.dowhere;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author: Jessica
 * @create: 2022-04-30 9:00
 * @desc:
 **/
@EnableDiscoveryClient
@SpringBootApplication
public class PaymentMain_9002 {
    public static void main(String[] args) {
        SpringApplication.run(PaymentMain_9002.class, args);
    }
}
