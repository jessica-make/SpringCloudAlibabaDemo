package com.dowhere.service;

/**
 * @author: Jessica
 * @create: 2022-04-26 20:20
 * @desc:
 **/

public interface PaymentService {
    /**
     * 正常访问，肯定没有问题的方法
     * @param id
     * @return
     */
    String paymentInfo_OK(Integer id);

    String paymentInfo_TimeOut(Integer id);

    String paymentCircuitBreaker(Integer id);

}
