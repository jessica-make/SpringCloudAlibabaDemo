package com.dowhere.service.impl;

import cn.hutool.core.util.IdUtil;
import com.dowhere.service.PaymentService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.concurrent.TimeUnit;

/**
 * @author: Jessica
 * @create: 2022-04-26 20:20
 * @desc:
 **/
@Service
public class PaymentServiceImpl implements PaymentService {

    /**
     * 正常访问，肯定没有问题的方法
     * @param id
     * @return
     */
    @Override
    public String paymentInfo_OK(Integer id) {
        return "线程池: "+Thread.currentThread().getName() + " paymentInfo_OK,id: " +id+ "\t"+ "^_^";
    }

    @HystrixCommand(fallbackMethod = "paymentInfo_TimeOutHandler", commandProperties = { //fallbackMethod：兜底方法
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "5000") //线程执行超时时间为3s,3s以内走正常逻辑，超过3s，调用指定方法
    })
    @Override
    public String paymentInfo_TimeOut(Integer id) {
        int timeNumber = 3;
        try {
            TimeUnit.SECONDS.sleep(timeNumber);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "线程池：" + Thread.currentThread().getName() + "paymentinfo_Timeout,id:" + id + "\t" + "耗时(秒)" + timeNumber;
    }


    public String paymentInfo_TimeOutHandler(Integer id) {
        return "调用支付接口超时或者异常：\t" + "\t当前线程："+Thread.currentThread().getName() + " paymentInfo_TimeOutHandler, id: " +id;
    }

    //===================服务熔断============================================

    //HystrixCommandProperties
    @HystrixCommand(fallbackMethod = "paymentCircuitBreaker_fallback", commandProperties = {//配置兜底方法
            @HystrixProperty(name = "circuitBreaker.enabled", value = "true"),//是否开启断路器
            @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold",value = "10"),//请求次数
            @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "10000"),//时间窗口期
            @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "60")//失败率达到多少后跳闸
    })//在一个时间窗口期内，10次请求的失败率达到60%就跳闸
    public String  paymentCircuitBreaker(@PathVariable("id") Integer id) {
        if (id < 0) {
            throw new RuntimeException("***********id 不能为负数");
        }
        String serialNumber = IdUtil.simpleUUID();//等价于UUID.randomUUID().toString()    commons中引入了hutool-all hutool.cn
        return Thread.currentThread().getName()+ "\t"+ "调用成功, 流水号: "+serialNumber;
    }

    public String paymentCircuitBreaker_fallback(@PathVariable("id") Integer id) {
        return "id 不能为负数，请稍后再试，id："+id;
    }

}
