package com.dowhere;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author: Jessica
 * @create: 2022-04-29 17:07
 * @desc:
 **/
@SpringBootApplication
public class StreamMQMain_8801 {
   public static void main(String[] args) {
      SpringApplication.run(StreamMQMain_8801.class,args);
   }
}
