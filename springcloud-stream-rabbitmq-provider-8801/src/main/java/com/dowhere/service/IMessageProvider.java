package com.dowhere.service;

/**
 * @author: Jessica
 * @create: 2022-04-29 17:19
 * @desc:
 **/

public interface IMessageProvider {
    String send();
}
