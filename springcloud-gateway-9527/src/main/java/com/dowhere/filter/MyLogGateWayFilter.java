package com.dowhere.filter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.Date;

/**
 * @author: Jessica
 * @create: 2022-04-27 20:58
 * @desc: 自定义全局过滤器
 **/
@Component
@Slf4j
public class MyLogGateWayFilter implements GlobalFilter, Ordered {
    /**
     * @param exchange 相当于 HttpServerRequest
     * @param chain
     *
     * curl http://localhost:9527/payment/lb?username=jessica --cookie "username=Jessica" -H "password:123"
     * @return
     */
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        log.info("********************come in MyLogGateWayFilter " + new Date());
        String username = exchange.getRequest().getQueryParams().getFirst("username");
        if (username == null) {
            log.info("*************用户名为null，非法用户");
            exchange.getResponse().setStatusCode(HttpStatus.NOT_ACCEPTABLE);
            return exchange.getResponse().setComplete();
        }
        //username不为空,去下一个过滤
        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        return 0;//加载过滤器的顺序，数字越小，优先级越高
    }
}
