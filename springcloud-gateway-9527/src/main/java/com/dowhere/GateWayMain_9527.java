package com.dowhere;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @author: Jessica
 * @create: 2022-04-27 17:19
 * @desc:
 **/

@SpringBootApplication
@EnableEurekaClient
public class GateWayMain_9527 {
    public static void main(String[] args) {
        SpringApplication.run(GateWayMain_9527.class, args);
    }
}
