package com.dowhere.config;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author: Jessica
 * @create: 2022-04-27 18:14
 * @desc:
 **/

@Configuration
public class GateWayConfig {

    @Bean
    public RouteLocator customRouteLocatoor(RouteLocatorBuilder routeLocatorBuilder) {
        RouteLocatorBuilder.Builder routes = routeLocatorBuilder.routes();

//        xxx时间后可以访问 时间戳
//        ZonedDateTime now = ZonedDateTime.now();
//        System.out.println("now "+now);

        /**
         * 配置了一个id为 path_route_Jessica的路由规则，
         * 当访问http://localhost:9527/guonei 将会转发到 http://news.baidu.com/guonei
         */
        routes.route("path_route_Jessica",
                r -> r.path("/guonei")
                        .uri("http://news.baidu.com/guonei"));
        return routes.build();
    }


    @Bean
    public RouteLocator customRouteLocatoor2(RouteLocatorBuilder routeLocatorBuilder) {
        RouteLocatorBuilder.Builder routes = routeLocatorBuilder.routes();
        routes.route("path_route_Jessica老哥",
                r -> r.path("/guoji")
                        .uri("http://news.baidu.com/guoji"));
        return routes.build();
    }
}

