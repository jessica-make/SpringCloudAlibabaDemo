1、curl 访问 不带cookie
curl http://localhost:9527/payment/lb

2、curl 访问 带cookie
curl http://localhost:9527/payment/lb --cookie "username=Jessica"

3、curl 访问 带cookie 以及 Header带参数
curl http://localhost:9527/payment/lb --cookie "username=Jessica" -H "password:123"