package com.dowhere.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author: Jessica
 * @create: 2022-04-24 18:14
 * @desc:
 **/

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Payment {
    private Long id;

    private String serial;
}
