package com.dowhere;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author: Jessica
 * @create: 2022-04-29 19:04
 * @desc:
 **/
@SpringBootApplication
public class StreamMQMain_8802 {
    public static void main(String[] args) {
        SpringApplication.run(StreamMQMain_8802.class, args);
    }
}
