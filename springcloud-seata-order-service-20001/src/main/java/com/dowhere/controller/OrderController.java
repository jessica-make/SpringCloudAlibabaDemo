package com.dowhere.controller;

import com.dowhere.entity.CommonResult;
import com.dowhere.entity.Order;
import com.dowhere.service.OrderService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author: Jessica
 * @create: 2022-05-02 11:53
 * @desc:
 **/
@RestController
public class OrderController {
    @Resource
    private OrderService orderService;

    @GetMapping("/order/create")
    public CommonResult<?> create(Order order) {
        orderService.create(order);
        return new CommonResult<Order>(200,"订单创建成功");
    }


}
