package com.dowhere.service;

import com.dowhere.entity.CommonResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author: Jessica
 * @create: 2022-05-02 11:51
 * @desc:
 **/
@FeignClient(value = "springcloudAlibaba-seata-storage-service")//调用微服务的名字
public interface StorageService {
    @PostMapping(value = "/storage/decrease")
    CommonResult<?> decrease(@RequestParam("productId") Long productId,
                          @RequestParam("count") Integer count);
}
