package com.dowhere.service;

import com.dowhere.entity.Order;

/**
 * @author: Jessica
 * @create: 2022-05-02 11:49
 * @desc:
 **/

public interface OrderService {
    void create(Order order);
}
