package com.dowhere.service;

import com.dowhere.entity.CommonResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;

/**
 * @author: Jessica
 * @create: 2022-05-02 11:50
 * @desc:
 **/
@FeignClient(value = "springcloudAlibaba-seata-account-service")
public interface AccountService {
    @PostMapping(value = "/account/decrease")
    CommonResult<?> decrease(@RequestParam("userId") Long userId,
                          @RequestParam("money") BigDecimal money);
}
