package com.dowhere.service.impl;

import com.dowhere.dao.OrderDao;
import com.dowhere.entity.Order;
import com.dowhere.idgen.IdGenerator;
import com.dowhere.service.AccountService;
import com.dowhere.service.OrderService;
import com.dowhere.service.StorageService;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author: Jessica
 * @create: 2022-05-02 11:52
 * @desc:
 **/

@Service
@Slf4j
public class OrderServiceImpl implements OrderService {

    @Resource
    private OrderDao orderDao;

    @Resource
    private StorageService storageService;

    @Resource
    private AccountService accountService;

    @Override
    @GlobalTransactional(name = "springcloudAlibaba-seata-order-service",rollbackFor = Exception.class)
    public void create(Order order) {
        // 1.新建订单
        log.info("-----> 开始新建订单");
        orderDao.create(order);
        log.info("-----> 开始新建订单end");
        // 2.扣减库存
        log.info("-----> 订单微服务开始调用库存，做扣减count操作");
        storageService.decrease(order.getProductId(),order.getCount());
        log.info("-----> 订单微服务开始调用库存，做扣减count操作end");
        // 3.扣减账户余额
        log.info("-----> 账户余额做money扣减");
        accountService.decrease(order.getUserId(),order.getMoney());
        log.info("-----> 账户余额做money扣减end");
        // .修改订单状态
        log.info("-----> 修改订单状态开始");
        orderDao.update(order.getUserId(),0);
        log.info("-----> 修改订单状态结束");

        log.info("订单已完成");
    }


    /**
     * 雪花案例
     */
    @Autowired
    private IdGenerator idGenerator;

    public String getIdGenerator(){
        ExecutorService executorService = Executors.newFixedThreadPool(5);
        for (int i = 1; i < 20; i++) {
            executorService.submit(()-> System.out.println(idGenerator.snowflakeId()));
            executorService.shutdown();
        }
        return "hello";
    }



}
