package com.dowhere.idgen;

import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.net.NetUtil;
import cn.hutool.core.util.IdUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author: Jessica
 * @create: 2022-05-03 9:16
 * @desc:
 **/
@Slf4j
@Component
public class IdGenerator {

    private long workerId = 0;
    private final Snowflake snowflake = IdUtil.createSnowflake(workerId,1);

    @PostConstruct
    void init() {
        try {
            workerId = NetUtil.ipv4ToLong(NetUtil.getLocalhostStr());
            log.info("当前机器 workerId: {}", workerId);
        } catch (Exception e) {
            log.error("获取机器 ID 失败", e);
            workerId = NetUtil.getLocalhost().hashCode();
            log.info("当前机器 workerId: {}", workerId);
        }
    }

    public synchronized long snowflakeId() {
        return snowflake.nextId();
    }

    public synchronized long snowflakeId(long workerId, long dataCenterId) {
        Snowflake snowflake = IdUtil.createSnowflake(workerId,dataCenterId);
        return snowflake.nextId();
    }

    public static void main(String[] args) {
        System.out.println(new IdGenerator().snowflakeId());

    }
}
