package com.dowhere.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author: Jessica
 * @create: 2022-05-02 19:00
 * @desc:
 **/
@Configuration
@MapperScan({"com.dowhere.dao"})
public class MyBatisConfig {

}
