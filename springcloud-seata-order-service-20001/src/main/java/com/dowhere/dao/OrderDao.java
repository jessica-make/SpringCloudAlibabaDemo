package com.dowhere.dao;

import com.dowhere.entity.Order;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author: Jessica
 * @create: 2022-05-02 11:48
 * @desc:
 **/
@Mapper
public interface OrderDao {
    // 新建订单
    void create(Order order);

    // 修改订单状态 从0改为1
    void update(@Param("userId") Long userId, @Param("status") Integer status);
}
