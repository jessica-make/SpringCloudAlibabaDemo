package com.dowhere.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

/**
 * @author: Jessica
 * @create: 2022-04-26 10:54
 * @desc:
 **/

@RestController
@Slf4j
public class ConsulConsumerController {
    private final static String PAYMENT_Zk_URL = "http://consul-provider-payment";

    @Resource
    private RestTemplate restTemplate;

    @RequestMapping("/consumer/consul")
    public String consumerConsul() {
        return restTemplate.getForObject(PAYMENT_Zk_URL+"/payment/consul",String.class);
    }
}
