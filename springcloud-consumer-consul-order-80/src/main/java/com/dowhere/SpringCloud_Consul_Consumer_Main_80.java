package com.dowhere;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author: Jessica
 * @create: 2022-04-26 10:53
 * @desc:
 **/
@SpringBootApplication
@EnableDiscoveryClient
public class SpringCloud_Consul_Consumer_Main_80 {
    public static void main(String[] args) {
        SpringApplication.run(SpringCloud_Consul_Consumer_Main_80.class,args);
    }
}
