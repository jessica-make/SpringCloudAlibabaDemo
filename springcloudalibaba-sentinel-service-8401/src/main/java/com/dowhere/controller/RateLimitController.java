package com.dowhere.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.dowhere.entity.CommonResult;
import com.dowhere.entity.Payment;
import com.dowhere.handler.CustomerBlockHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: Jessica
 * @create: 2022-05-01 17:56
 * @desc:
 **/
@RestController
public class RateLimitController {
    @GetMapping("/byResource")
    @SentinelResource(value = "byResource", blockHandler = "handleException")
    public CommonResult byResource() {
        return new CommonResult(200,"按资源名称限流测试ok", new Payment(2022L, "serial001"));
    }

    public CommonResult handleException(BlockException exception) {
        return new CommonResult(444, exception.getClass().getCanonicalName() + "\t 服务不可用");
    }

    /**
     *************************************************
     */

    @GetMapping("/rateLimit/byUrl")
    @SentinelResource(value = "byUrl")
    public CommonResult byUrl() {
        return new CommonResult(200, "按url限流测试ok", new Payment(2022L, "serial002"));
    }


    @GetMapping("/rateLimit/handlerException")
    @SentinelResource(value = "handlerException",
            blockHandlerClass = CustomerBlockHandler.class,
            blockHandler = "handlerException")
    public CommonResult byhandlerException() {
        return new CommonResult(200, "按handlerException限流测试ok", new Payment(2022L, "serial002"));
    }

    @GetMapping("/rateLimit/handlerException2")
    @SentinelResource(value = "handlerException2",
            blockHandlerClass = CustomerBlockHandler.class,
            blockHandler = "handlerException2")
    public CommonResult handlerException2() {
        return new CommonResult(200, "按handlerException2限流测试ok", new Payment(2022L, "serial002"));
    }




}
