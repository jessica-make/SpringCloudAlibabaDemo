package com.dowhere.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

/**
 * @author: Jessica
 * @create: 2022-05-01 8:10
 * @desc:
 **/
@RestController
@Slf4j
public class FlowLimitController {

    @GetMapping("/testA")
    public String testA() {
        log.info(Thread.currentThread().getName()+"\t"+" ....testA");
        return "-----------A";
    }

    @GetMapping("/testB")
    public String testB() {
        return "-----------B";
    }


    @GetMapping("/testD")
    public String testD() throws InterruptedException {
        TimeUnit.SECONDS.sleep(1);
        log.info("testD ---测试RT");
        return "---testD";
    }

    @GetMapping("/testHotKey")  //rest地址
    @SentinelResource(value = "testHotKey", blockHandler = "deal_testHotKey")
    public String testHotKey(@RequestParam(value = "p1", required = false)String p1,
                             @RequestParam(value = "p2", required = false)String p2 ) {
        return "------ testHotKey";
    }


    public String deal_testHotKey(String p1, String p2, BlockException exception) {
        return "------  deal_testHotKey";//sentinel 的默认提示都是Blocked by Sentinel (flow limiting)
    }


}
