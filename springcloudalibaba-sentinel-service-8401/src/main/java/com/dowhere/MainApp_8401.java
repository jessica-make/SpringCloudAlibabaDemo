package com.dowhere;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author: Jessica
 * @create: 2022-05-01 8:08
 * @desc:
 **/
@EnableDiscoveryClient
@SpringBootApplication
public class MainApp_8401 {
    public static void main(String[] args) {
        SpringApplication.run(MainApp_8401.class);
    }
}
