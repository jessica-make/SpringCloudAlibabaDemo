package com.dowhere.handler;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.dowhere.entity.CommonResult;

/**
 * @author: Jessica
 * @create: 2022-05-01 18:09
 * @desc:
 **/

public class CustomerBlockHandler {
    public static CommonResult handlerException(BlockException exception) {
        return new CommonResult(444, "按客户自定义, global handlerException----1");
    }

    public static CommonResult handlerException2(BlockException exception) {
        return new CommonResult(444, "按客户自定义, global handlerException-----2");
    }
}
