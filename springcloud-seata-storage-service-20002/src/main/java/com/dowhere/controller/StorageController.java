package com.dowhere.controller;

import com.dowhere.entity.CommonResult;
import com.dowhere.service.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: Jessica
 * @create: 2022-05-02 13:16
 * @desc:
 **/

@RestController
public class StorageController {
    @Autowired
    private StorageService storageService;

    /**
     * 扣减库存
     */
    @RequestMapping("/storage/decrease")
    public CommonResult<?> decrease(Long productId, Integer count) {
        storageService.decrease(productId,count);
        return new CommonResult(200,"扣减库存成功");
    }
}
