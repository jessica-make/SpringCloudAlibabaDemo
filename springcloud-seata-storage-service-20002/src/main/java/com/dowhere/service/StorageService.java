package com.dowhere.service;

/**
 * @author: Jessica
 * @create: 2022-05-02 13:14
 * @desc:
 **/

public interface StorageService {
    /**
     * 扣减库存
     */
    void decrease(Long productId, Integer count);
}
