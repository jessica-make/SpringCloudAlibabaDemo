package com.dowhere.service.impl;

import com.dowhere.dao.StorageDao;
import com.dowhere.service.StorageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service

/**
 * @author: Jessica
 * @create: 2022-05-02 13:14
 * @desc:
 **/

public class StorageServiceImpl implements StorageService {
    private static final Logger logger = LoggerFactory.getLogger(StorageServiceImpl.class);

    @Resource
    private StorageDao storageDao;

    @Override
    public void decrease(Long productId, Integer count) {
        logger.info("------>storage-service开始扣减库存");
        storageDao.decrease(productId,count);
        logger.info("------>storage-service扣减库存结束");
    }
}
