package com.dowhere.service;

import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;

/**
 * @author: Jessica
 * @create: 2022-05-02 13:24
 * @desc:
 **/

public interface AccountService {
    void decrease(@RequestParam("userId") Long userId,
                  @RequestParam("money") BigDecimal money) throws Exception;
}
