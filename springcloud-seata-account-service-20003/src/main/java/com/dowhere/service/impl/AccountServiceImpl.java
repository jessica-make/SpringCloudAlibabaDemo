package com.dowhere.service.impl;

import com.dowhere.dao.AccountDao;
import com.dowhere.service.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.concurrent.TimeUnit;


/**
 * @author: Jessica
 * @create: 2022-05-02 13:25
 * @desc:
 **/

@Service
public class AccountServiceImpl implements AccountService {
    private static final Logger logger = LoggerFactory.getLogger(AccountServiceImpl.class);

    @Resource
    AccountDao accountDao;

    @Override
    public void decrease(Long userId, BigDecimal money) throws Exception{
        logger.info("------> account-service 中开始扣减账户余额");

        /**
         * 模拟超时
         */

            TimeUnit.SECONDS.sleep(20);

        accountDao.decrease(userId,money);
        logger.info("------> account-service 中扣减账户余额结束");
    }
}
