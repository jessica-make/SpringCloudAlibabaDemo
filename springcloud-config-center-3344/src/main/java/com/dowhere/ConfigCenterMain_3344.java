package com.dowhere;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

/**
 * @author: Jessica
 * @create: 2022-04-28 7:50
 * @desc:
 **/

@SpringBootApplication
@EnableConfigServer
public class ConfigCenterMain_3344 {
    public static void main(String[] args) {
        SpringApplication.run(ConfigCenterMain_3344.class);
    }
}
